#! /bin/bash

# If the script isn't in the PATH, replace the following with its absolute path
call=genericXMCDAService.py

$call -U http://webservices-test-v3.decision-deck.org/soap/%s.py \
      -n SRMP-R-MCDA \
      --submit-and-wait-solution \
      alternatives:tests/in1.v3/alternatives.xml \
      criteria:tests/in1.v3/criteria.xml \
      criteriaWeights:tests/in1.v3/criteriaWeights.xml \
      lexicographicOrder:tests/in1.v3/lexicographicOrder.xml \
      performanceTable:tests/in1.v3/performanceTable.xml \
      referenceProfiles:tests/in1.v3/referenceProfiles.xml \
      referenceProfilesPerformanceTable:tests/in1.v3/referenceProfilesPerformanceTable.xml

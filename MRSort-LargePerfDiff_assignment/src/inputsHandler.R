checkAndExtractInputs <- function(xmcdaData, programExecutionResult) {
    # all parameters in the order in which the R MCDA function takes them

    performanceTable <- NULL
    categoriesLowerProfiles <- NULL
    categoriesRanks <- NULL
    criteriaWeights <- NULL
    criteriaMinMax <- NULL
    majorityThreshold <- NULL
    criteriaVetos <- NULL
    criteriaDictators <- NULL
    assignmentRule <- NULL
    alternativesIDs <- NULL
    criteriaIDs <- NULL
    categoriesIDs <- NULL

    #############################################
    # get criteria
    #############################################
    
    criteriaIDs <- getActiveCriteria(xmcdaData)$criteriaIDs
    
    #############################################
    # get weights
    #############################################
    
    criteriaWeights <- getNumericCriteriaValuesList(xmcdaData)[[1]]
    
    #############################################
    # get preference directions
    #############################################
    
    criteriaMinMax <- getCriteriaPreferenceDirectionsList(xmcdaData)[[1]]
    
    #############################################
    # get categories
    #############################################

    categoriesIDs <- getActiveCategories(xmcdaData)$categoriesIDs

    #############################################
    # get performance table
    #############################################
    
    performanceTableList <- getNumericPerformanceTableList(xmcdaData)
    
    # we assume that the first performance table is the actual performance table
    
    if(length(performanceTableList) == 0)
      stop("Error: no performance table supplied")
    
    performanceTable <- performanceTableList[[1]]
    
    #############################################
    # get alternatives
    #############################################
    
    activeIDs <- getActiveAlternatives(xmcdaData)$alternativesIDs
    
    alternativesIDs <- rownames(performanceTable)[rownames(performanceTable) %in% activeIDs]
    
    #############################################
    # get majority threshold - and assignment rule if it exists
    #############################################
    
    parameters <- getProgramParametersList(xmcdaData)
    
    if(length(parameters) == 0)
      stop("No method parameters provided.")
    else
    {
      for(i in 1:length(parameters))
      {
        param_name <- names(parameters[[i]])[1]
        if(param_name == "majority")
          majorityThreshold <- parameters[[i]]$majority[[1]]
        else if(param_name == "assigrule")
          assignmentRule <- parameters[[i]]$assigrule[[1]]
        else if(param_name == "majorityGUI")
        {
          if(is.null(majorityThreshold))
            majorityThreshold <- parameters[[i]]$majorityGUI[[1]]
        }
        else if(param_name == "assigruleGUI")
        {
          if(is.null(assignmentRule))
            assignmentRule <- parameters[[i]]$assigruleGUI[[1]]
        }
      }
    }

    if(is.null(majorityThreshold))
      stop("No majority threshold provided")
    
    # default majority rule to M if one was not provided
    
    if(is.null(assignmentRule))
      assignmentRule <- 'M'

    #############################################
    # get category profiles
    #############################################
    
    categoriesLowerProfilesPerformanceTableList <- getNumericCategoriesLowerProfilesPerformanceTableList(xmcdaData)
    
    if("bounding" %in% names(categoriesLowerProfilesPerformanceTableList))
      categoriesLowerProfiles <- categoriesLowerProfilesPerformanceTableList$bounding
    else
      stop("Error: no categories lower profiles have been provided (performance table too)")
    
    if("veto" %in% names(categoriesLowerProfilesPerformanceTableList))
    {
        criteriaVetos <- matrix(rep(NA,length(criteriaIDs) * length(categoriesIDs)), length(categoriesIDs), length(criteriaIDs))
        rownames(criteriaVetos) <- categoriesIDs
        colnames(criteriaVetos) <- criteriaIDs

        vetoes <- categoriesLowerProfilesPerformanceTableList$veto
        
        for(i in rownames(vetoes))
            for(j in colnames(vetoes))
                criteriaVetos[i,j] <- vetoes[i,j]
    }
    else
    {
      if(assignmentRule %in% c('V','v','dV','Dv','dv'))
        stop("Error: no categories veto profiles have been provided when the provided assignmentRule requires them")
    }
    
    if("dictator" %in% names(categoriesLowerProfilesPerformanceTableList))
      criteriaDictators <- categoriesLowerProfilesPerformanceTableList$dictator
    if("dictator" %in% names(categoriesLowerProfilesPerformanceTableList))
    {
        criteriaDictators <- matrix(rep(NA,length(criteriaIDs) * length(categoriesIDs)), length(categoriesIDs), length(criteriaIDs))
        rownames(criteriaDictators) <- categoriesIDs
        colnames(criteriaDictators) <- criteriaIDs

        dictators <- categoriesLowerProfilesPerformanceTableList$dictator
        
        for(i in rownames(dictators))
            for(j in colnames(dictators))
                criteriaDictators[i,j] <- dictators[i,j]
    }
    else
    {
      if(assignmentRule %in% c('D','d','dV','Dv','dv'))
        stop("Error: no categories dictator profiles have been provided when the provided assignmentRule requires them")
    }
    
    #############################################
    # get categories ranks
    #############################################

    categoriesValuesList <- getCategoriesValuesList(xmcdaData)
    
    if(length(categoriesValuesList) == 0)
      stop("Error: no categories values supplied")
    
    categoriesRanks <- categoriesValuesList[[1]]
    
    if(!all(sort(categoriesRanks) == 1:length(categoriesIDs)))
      stop('Error: categories ranks should be from 1 to the number of categories')
    
    return(list(performanceTable = performanceTable, categoriesLowerProfiles = categoriesLowerProfiles, categoriesRanks = categoriesRanks, criteriaWeights = criteriaWeights, criteriaMinMax = criteriaMinMax, majorityThreshold = majorityThreshold, criteriaVetos = criteriaVetos, criteriaDictators = criteriaDictators, majorityRule = assignmentRule, alternativesIDs = alternativesIDs, criteriaIDs = criteriaIDs, categoriesIDs = categoriesIDs))
}

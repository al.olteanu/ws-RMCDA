#! /bin/bash

# If the script isn't in the PATH, replace the following with its absolute path
call=genericXMCDAService.py

$call -U http://webservices-test-v3.decision-deck.org/soap/%s.py \
      -n MRSortLPDInferenceApprox-R-MCDA \
      --submit-and-wait-solution \
      alternatives:tests/in1.v3/alternatives.xml \
      alternativesAssignments:tests/in1.v3/alternativesAssignments.xml \
      assignmentRule:tests/in1.v3/assignmentRule.xml \
      categoriesRanks:tests/in1.v3/categoriesRanks.xml \
      criteria:tests/in1.v3/criteria.xml \
      mutation:tests/in1.v3/mutation.xml \
      performanceTable:tests/in1.v3/performanceTable.xml \
      population:tests/in1.v3/population.xml \
      time:tests/in1.v3/time.xml

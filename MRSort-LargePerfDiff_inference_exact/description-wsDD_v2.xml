<?xml version='1.0' encoding='UTF-8'?>
<program_description>
	<program provider="R-MCDA" name="MRSort-LargePerfDiff_inference_exact" version="1.0" displayName="Exact inference of MRSort with large performance differences" />
	<documentation>
		<description>MRSort is a simplified ELECTRE TRI sorting method, where alternatives are assigned to an ordered set of categories. In this case, we also take into account large performance differences, both negative (vetoes) and positive (dictators). The identification of the profiles, weights and majority threshold are done by taking into account assignment examples.</description>
		<contact><![CDATA[Alexandru Olteanu (alexandru.olteanu@univ-ubs.fr)]]></contact>
		<reference>P. MEYER, A-L. OLTEANU, Integrating large positive and negative performance differences into multicriteria majority-rule sorting models, Computers and Operations Research, 81, pp. 216 - 230, 2017.</reference>
	</documentation>
	<parameters>

		<input id="incrit" name="criteria" displayName="criteria scales" isoptional="0">
			<documentation>
				<description>A list of criteria on which the alternatives are evaluated. For each criterion, the preference direction should be provided.</description>
			</documentation>
			<xmcda tag="criteria" />
		</input>

		<input id="inalt" name="alternatives" displayName="alternatives" isoptional="0">
			<documentation>
				<description>A complete list of alternatives to be considered when inferring the MR-Sort model.</description>
			</documentation>
			<xmcda tag="alternatives"><![CDATA[
			]]></xmcda>
		</input>

		<input id="inperf" name="performanceTable" displayName="performance table" isoptional="0">
			<documentation>
				<description>The evaluations of the alternatives on the set of criteria.</description>
			</documentation>
			<xmcda tag="performanceTable" />
		</input>

		<input id="assignments" name="alternativesAssignments" displayName="alternatives assignments" isoptional="0">
			<documentation>
				<description>The alternatives assignments to categories.</description>
			</documentation>
			<xmcda tag="alternativesAffectations" />
		</input>

		<input id="incategval" name="categoriesRanks" displayName="categories ranks" isoptional="0">
			<documentation>
				<description>A list of categories ranks, 1 stands for the most preferred category and the higher the number the lower the preference for that category.</description>
			</documentation>
			<xmcda tag="categoriesValues" />
		</input>

		<input id="parameters" name="parameters" displayName="parameters" isoptional="defaultTrue">
			<documentation>
				<description>The program parameters.</description>
			</documentation>
			<xmcda tag="methodParameters"><![CDATA[
			   
    <methodParameters>
        <parameter id="assignmentRule">
            <value>
                <label>%1</label>
            </value>
        </parameter>
        <parameter id="readableWeights">
            <value>
                <boolean>%2</boolean>
            </value>
        </parameter>
        <parameter id="readableProfiles">
            <value>
                <boolean>%3</boolean>
            </value>
        </parameter>
        <parameter id="minmaxLPDProfiles">
            <value>
                <boolean>%4</boolean>
            </value>
        </parameter>
    </methodParameters>

			]]></xmcda>
			<gui status="preferGUI">
				<entry id="%1" type="enum" displayName="Include vetoes">
			        <documentation>
				        <description>The type of assignment rule. Can be anything from the list M (majority rule), V (veto), D (dictator), v (veto weakened by dictator), d (dictator weakened by veto), dV (dominating veto and weakened dictator), Dv (dominating dictator and weakened veto) and dv (conflicting veto and dictator).</description>
			        </documentation>
					<items>
						<item id="M">
							<description>Majority rule</description>
							<value>M</value>
						</item>
						<item id="V">
							<description>Veto</description>
							<value>V</value>
						</item>
						<item id="D">
							<description>Dictator</description>
							<value>D</value>
						</item>
						<item id="v">
							<description>Veto weakened by dictator</description>
							<value>v</value>
						</item>
						<item id="d">
							<description>Dictator weakened by veto</description>
							<value>d</value>
						</item>
						<item id="dV">
							<description>Dominating Veto and weakened Dictator</description>
							<value>dV</value>
						</item>
						<item id="Dv">
							<description>Dominating Dictator and weakened veto</description>
							<value>Dv</value>
						</item>
						<item id="dv">
							<description>Conflicting Veto and Dictator</description>
							<value>dv</value>
						</item>
					</items>
					<defaultValue>M</defaultValue>
				</entry>
				<entry id="%2" type="boolean" displayName="Readable weights">
					<documentation>
						<description>An indicator for whether the weights should made easier to read.</description>
					</documentation>
					<defaultValue>false</defaultValue>
				</entry>
				<entry id="%3" type="boolean" displayName="Readable profiles">
					<documentation>
						<description>An indicator for whether the profiles should made easier to read.</description>
					</documentation>
					<defaultValue>false</defaultValue>
				</entry>
				<entry id="%4" type="boolean" displayName="Limit the use of large performance differences">
					<documentation>
						<description>An indicator for whether the method should try to reduce the use of veto and dictator profiles.</description>
					</documentation>
					<defaultValue>false</defaultValue>
				</entry>
			</gui>
		</input>

		<output id="outcatprofpt" name="categoriesProfilesPerformanceTable" displayName="categories profiles performancetable">
			<documentation>
				<description>The evaluations of the category profiles.</description>
			</documentation>
			<xmcda tag="performanceTable" />
		</output>

		<output id="outcatvetopt" name="vetoProfilesPerformanceTable" displayName="veto profiles performance table">
			<documentation>
				<description>The evaluations of the veto profiles.</description>
			</documentation>
			<xmcda tag="performanceTable" />
		</output>

		<output id="outcatdictatorpt" name="dictatorProfilesPerformanceTable" displayName="dictator profiles performance table">
			<documentation>
				<description>The evaluations of the dictator profiles.</description>
			</documentation>
			<xmcda tag="performanceTable" />
		</output>

		<output id="weights" name="criteriaWeights" displayName="criteria weights">
			<documentation>
				<description>The criteria weights.</description>
			</documentation>
			<xmcda tag="criteriaValues" />
		</output>

		<output id="outcatprof" name="categoriesProfiles" displayName="categories profiles">
			<documentation>
				<description>The categories delimiting profiles.</description>
			</documentation>
			<xmcda tag="categoriesProfiles" />
		</output>

		<output id="outcatveto" name="vetoProfiles" displayName="veto profiles">
			<documentation>
				<description>The categories veto profiles.</description>
			</documentation>
			<xmcda tag="categoriesProfiles" />
		</output>

		<output id="outcatdictator" name="dictatorProfiles" displayName="dictator profiles">
			<documentation>
				<description>The categories dictator profiles.</description>
			</documentation>
			<xmcda tag="categoriesProfiles" />
		</output>

		<output id="majority" name="majorityThreshold" displayName="majority threshold">
			<documentation>
				<description>The majority threshold.</description>
			</documentation>
			<xmcda tag="methodParameters" />
		</output>

		<output id="msg" name="messages" displayName="messages">
			<documentation>
				<description>Messages from the execution of the webservice. Possible errors in the input data will be given here.</description>
			</documentation>
			<xmcda tag="methodMessages" />
		</output>

	</parameters>
</program_description>

# TODO depending on whether the file was generated from a description based on
# XMCDA v2 or v3, only one list is correct, either XMCDA_v2_TAG_FOR_FILENAME
# or XMCDA_v3_TAG_FOR_FILENAME: check them to determine which one should be
# adapted.

XMCDA_v2_TAG_FOR_FILENAME <- list(
  # output name -> XMCDA v2 tag
  majorityThreshold <- "methodParameters",
  criteriaWeights <- "criteriaValues",
  categoriesProfiles <- "categoriesProfiles",
  categoriesProfilesPerformanceTable <- "performanceTable",
  vetoProfiles <- "categoriesProfiles",
  vetoProfilesPerformanceTable <- "performanceTable",
  dictatorProfiles <- "categoriesProfiles",
  dictatorProfilesPerformanceTable <- "performanceTable",
  messages = "methodMessages"
)

XMCDA_v3_TAG_FOR_FILENAME <- list(
  # output name -> XMCDA v3 tag
  majorityThreshold <- "programParameters",
  criteriaWeights <- "criteriaValues",
  categoriesProfiles <- "categoriesProfiles",
  categoriesProfilesPerformanceTable <- "performanceTable",
  vetoProfiles <- "categoriesProfiles",
  vetoProfilesPerformanceTable <- "performanceTable",
  dictatorProfiles <- "categoriesProfiles",
  dictatorProfilesPerformanceTable <- "performanceTable",
  messages = "programExecutionResult"
)

xmcda_v3_tag <- function(outputName){
  return (XMCDA_v3_TAG_FOR_FILENAME[[outputName]])
}

xmcda_v2_tag <- function(outputName){
  return (XMCDA_v2_TAG_FOR_FILENAME[[outputName]])
}


convert <- function(results, programExecutionResult) {
  xmcdaMajorityThreshold<-.jnew("org/xmcda/XMCDA")
  xmcdaCriteriaWeights<-.jnew("org/xmcda/XMCDA")
  xmcdaCategoriesLowerProfiles<-.jnew("org/xmcda/XMCDA")
  xmcdaCategoriesLowerProfilesPerformances<-.jnew("org/xmcda/XMCDA")
  xmcdaCategoriesVetoes<-.jnew("org/xmcda/XMCDA")
  xmcdaCategoriesVetoPerformances<-.jnew("org/xmcda/XMCDA")
  xmcdaCategoriesDictators<-.jnew("org/xmcda/XMCDA")
  xmcdaCategoriesDictatorPerformances<-.jnew("org/xmcda/XMCDA")
  
  # filter null or na profiles
  
  categoriesLowerProfiles <- c()
  categoriesLowerProfilesPerformanceTable <- c()
  for(i in 1:dim(results$profilesPerformances)[1])
  {
    if(!all(is.na(results$profilesPerformances[i,]) | is.null(results$profilesPerformances[i,])))
    {
      categoriesLowerProfiles <- c(categoriesLowerProfiles, paste("p",as.character(i),sep=""))
      
      names(categoriesLowerProfiles)[length(categoriesLowerProfiles)] <- rownames(results$profilesPerformances)[i]
      
      categoriesLowerProfilesPerformanceTable <- rbind(categoriesLowerProfilesPerformanceTable,results$profilesPerformances[i,])
      
      rownames(categoriesLowerProfilesPerformanceTable)[length(categoriesLowerProfiles)] <- categoriesLowerProfiles[length(categoriesLowerProfiles)]
    }
  }
  
  if("vetoPerformances" %in% names(results))
  {
    vetoProfiles <- c()
    vetoProfilesPerformanceTable <- c()
    for(i in 1:dim(results$vetoPerformances)[1])
    {
      if(!all(is.na(results$vetoPerformances[i,]) | is.null(results$vetoPerformances[i,])))
      {
        vetoProfiles <- c(vetoProfiles, paste("v",as.character(i),sep=""))
        
        names(vetoProfiles)[length(vetoProfiles)] <- rownames(results$vetoPerformances)[i]
        
        vetoProfilesPerformanceTable <- rbind(vetoProfilesPerformanceTable,results$vetoPerformances[i,])
        
        rownames(vetoProfilesPerformanceTable)[length(vetoProfiles)] <- vetoProfiles[length(vetoProfiles)]
      }
    }
  }

  if("dictatorPerformances" %in% names(results))
  {
    dictatorProfiles <- c()
    dictatorProfilesPerformanceTable <- c()
    for(i in 1:dim(results$dictatorPerformances)[1])
    {
      if(!all(is.na(results$dictatorPerformances[i,]) | is.null(results$dictatorPerformances[i,])))
      {
        dictatorProfiles <- c(dictatorProfiles, paste("d",as.character(i),sep=""))
        
        names(dictatorProfiles)[length(dictatorProfiles)] <- rownames(results$dictatorPerformances)[i]
        
        dictatorProfilesPerformanceTable <- rbind(dictatorProfilesPerformanceTable,results$dictatorPerformances[i,])
        
        rownames(dictatorProfilesPerformanceTable)[length(dictatorProfiles)] <- dictatorProfiles[length(dictatorProfiles)]
      }
    }
  }
  
  tmp<-handleException(
    function() return(c(
      putProgramParameters(xmcdaMajorityThreshold,"majority", results$majorityThreshold),
      putCriteriaValues(xmcdaCriteriaWeights,results$criteriaWeights),
      putCategoryProfiles(xmcdaCategoriesLowerProfiles,categoriesLowerProfiles),
      putNumericPerformanceTable(xmcdaCategoriesLowerProfilesPerformances, categoriesLowerProfilesPerformanceTable, "lower bound"),
      if("vetoPerformances" %in% names(results)) putCategoryProfiles(xmcdaCategoriesVetoes,vetoProfiles) else NULL,
      if("vetoPerformances" %in% names(results)) putNumericPerformanceTable(xmcdaCategoriesVetoPerformances, vetoProfilesPerformanceTable, "veto") else NULL,
      if("dictatorPerformances" %in% names(results)) putCategoryProfiles(xmcdaCategoriesDictators,dictatorProfiles) else NULL,
      if("dictatorPerformances" %in% names(results)) putNumericPerformanceTable(xmcdaCategoriesDictatorPerformances, dictatorProfilesPerformanceTable, "dictator") else NULL
      )
    ),
    programExecutionResult,
    humanMessage = "Could not put results in tree, reason: "
  )
  
  # if an error occurs, return null, else a dictionnary "xmcdaTag -> xmcdaObject"
  
  if (is.null(tmp)){
    return(null)
  } else{
    return (list(majority = xmcdaMajorityThreshold, weights = xmcdaCriteriaWeights, categoriesProfiles = xmcdaCategoriesLowerProfiles, categoriesProfilesPerformanceTable = xmcdaCategoriesLowerProfilesPerformances, vetoProfiles = xmcdaCategoriesVetoes, vetoProfilesPerformanceTable = xmcdaCategoriesVetoPerformances, dictatorProfiles = xmcdaCategoriesDictators, dictatorProfilesPerformanceTable = xmcdaCategoriesDictatorPerformances))
  }
  

}

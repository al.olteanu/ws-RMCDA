#! /bin/bash

# If the script isn't in the PATH, replace the following with its absolute path
call=genericXMCDAService.py

$call -U http://webservices-test-v3.decision-deck.org/soap/%s.py \
      -n MRSortLPDInferenceExact-R-MCDA \
      --submit-and-wait-solution \
      alternatives:tests/in1.v3/alternatives.xml \
      alternativesAssignments:tests/in1.v3/alternativesAssignments.xml \
      categoriesRanks:tests/in1.v3/categoriesRanks.xml \
      criteria:tests/in1.v3/criteria.xml \
      majorityRule:tests/in1.v3/majorityRule.xml \
      minmaxLPDProfiles:tests/in1.v3/minmaxLPDProfiles.xml \
      performanceTable:tests/in1.v3/performanceTable.xml \
      readableProfiles:tests/in1.v3/readableProfiles.xml \
      readableWeights:tests/in1.v3/readableWeights.xml

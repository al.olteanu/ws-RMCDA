<?xml version='1.0' encoding='UTF-8'?>
<program_description>
	<program provider="R-MCDA" name="MRSort_assignment" version="1.0" displayName="MRSort_assignment" />
	<documentation>
		<description>This simplification of the Electre TRI method uses the pessimistic assignment rule, without indifference or preference thresholds attached to criteria.
Only a binary discordance condition is considered, i.e. a veto forbids an outranking in any possible concordance situation, or not.</description>
		<contact><![CDATA[Alexandru Olteanu (alexandru.olteanu@univ-ubs.fr)]]></contact>
		<reference>Bouyssou, D. and Marchant, T. An axiomatic approach to noncompensatory sorting methods in MCDM, II: more than two categories. European Journal of Operational Research, 178(1): 246--276, 2007.</reference>
	</documentation>
	<parameters>

		<input id="incrit" name="criteria" displayName="criteria scales" isoptional="0">
			<documentation>
				<description>A list of criteria on which the alternatives are evaluated. For each criterion, the preference direction should be provided.</description>
			</documentation>
			<xmcda tag="criteria" />
		</input>

		<input id="inalt" name="alternatives" displayName="alternatives" isoptional="0">
			<documentation>
				<description>A complete list of alternatives to be considered by the MR-Sort method.</description>
			</documentation>
			<xmcda tag="alternatives" />
		</input>

		<input id="incateg" name="categories" displayName="categories" isoptional="0">
			<documentation>
				<description>A list of categories to which the alternatives will be assigned.</description>
			</documentation>
			<xmcda tag="categories" />
		</input>

		<input id="inperf" name="performanceTable" displayName="performance table" isoptional="0">
			<documentation>
				<description>The evaluations of the alternatives on the set of criteria.</description>
			</documentation>
			<xmcda tag="performanceTable" />
		</input>

		<input id="incatprofpt" name="categoriesProfilesPerformanceTable" displayName="categories profiles performance table" isoptional="0">
			<documentation>
				<description>The evaluations of the category profiles.</description>
			</documentation>
			<xmcda tag="performanceTable" />
		</input>

		<input id="invetoprofpt" name="vetoProfilesPerformanceTable" displayName="veto profiles performance table" isoptional="defaultTrue">
			<documentation>
				<description>The evaluations of the veto profiles.</description>
			</documentation>
			<xmcda tag="performanceTable" />
		</input>

		<input id="weights" name="criteriaWeights" displayName="criteria weights" isoptional="0">
			<documentation>
				<description>The criteria weights.</description>
			</documentation>
			<xmcda tag="criteriaValues" />
		</input>

		<input id="incatprof" name="categoriesProfiles" displayName="categories profiles" isoptional="0">
			<documentation>
				<description>The categories delimiting profiles.</description>
			</documentation>
			<xmcda tag="categoriesProfiles" />
		</input>

		<input id="invetoprof" name="vetoProfiles" displayName="veto profiles" isoptional="defaultTrue">
			<documentation>
				<description>The categories veto profiles.</description>
			</documentation>
			<xmcda tag="categoriesProfiles" />
		</input>

		<input id="incategval" name="categoriesRanks" displayName="categories ranks" isoptional="0">
			<documentation>
				<description>A list of categories ranks, 1 stands for the most preferred category and the higher the number the lower the preference for that category.</description>
			</documentation>
			<xmcda tag="categoriesValues" />
		</input>

		<input id="parameters" name="parameters" displayName="Parameters" isoptional="0">
			<documentation>
				<description>The method parameters.</description>
			</documentation>
			<xmcda tag="methodParameters"><![CDATA[
    <methodParameters>
        <parameter id="majorityGUI">
            <value>
                <real>%1</real>
            </value>
        </parameter>
    </methodParameters>
			]]></xmcda>
			<gui status="preferGUI">
				<entry id="%1" type="float" displayName="Majority threshold">
					<documentation>
						<description>The majority threshold needed to validate the outranking relation.</description>
					</documentation>
					<constraint>
						<description>A value between 0.5 and 1</description>
						<code><![CDATA[ %1 >= 0.5 && %1 <= 1 ]]></code>
					</constraint>
					<defaultValue>0.5</defaultValue>
				</entry>
			</gui>
		</input>

		<input id="majority" name="majorityThreshold" displayName="Majority threshold (file input)" isoptional="defaultTrue">
			<documentation>
				<description>The majority threshold (provided as a separate file). Overrides the value provided in the parameters window.</description>
			</documentation>
			<xmcda tag="methodParameters"/>
		</input>

		<output id="outaffect" name="alternativesAssignments" displayName="alternatives assignments">
			<documentation>
				<description>The alternatives assignments to categories.</description>
			</documentation>
			<xmcda tag="alternativesAffectations" />
		</output>

		<output id="msg" name="messages" displayName="messages">
			<documentation>
				<description>Messages from the execution of the webservice. Possible errors in the input data will be given here.</description>
			</documentation>
			<xmcda tag="methodMessages" />
		</output>

	</parameters>
</program_description>

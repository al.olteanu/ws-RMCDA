#! /bin/bash

# If the script isn't in the PATH, replace the following with its absolute path
call=genericXMCDAService.py

$call -U http://webservices-test-v3.decision-deck.org/soap/%s.py \
      -n MRSort-R-MCDA \
      --submit-and-wait-solution \
      alternatives:tests/in1.v3/alternatives.xml \
      categories:tests/in1.v3/categories.xml \
      categoriesProfiles:tests/in1.v3/categoriesProfiles.xml \
      categoriesProfilesPerformanceTable:tests/in1.v3/categoriesProfilesPerformanceTable.xml \
      categoriesRanks:tests/in1.v3/categoriesRanks.xml \
      criteria:tests/in1.v3/criteria.xml \
      criteriaWeights:tests/in1.v3/criteriaWeights.xml \
      majorityThreshold:tests/in1.v3/majorityThreshold.xml \
      performanceTable:tests/in1.v3/performanceTable.xml \
      vetoProfiles:tests/in1.v3/vetoProfiles.xml \
      vetoProfilesPerformanceTable:tests/in1.v3/vetoProfilesPerformanceTable.xml

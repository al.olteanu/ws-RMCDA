#! /bin/bash

# If the script isn't in the PATH, replace the following with its absolute path
call=genericXMCDAService.py

$call -U http://webservices-test-v3.decision-deck.org/soap/%s.py \
      -n MRSortInferenceExact-R-MCDA \
      --submit-and-wait-solution \
      alternatives:tests/in1.v3/alternatives.xml \
      alternativesAssignments:tests/in1.v3/alternativesAssignments.xml \
      categoriesRanks:tests/in1.v3/categoriesRanks.xml \
      criteria:tests/in1.v3/criteria.xml \
      performanceTable:tests/in1.v3/performanceTable.xml \
      readableProfiles:tests/in1.v3/readableProfiles.xml \
      readableWeights:tests/in1.v3/readableWeights.xml \
      veto:tests/in1.v3/veto.xml

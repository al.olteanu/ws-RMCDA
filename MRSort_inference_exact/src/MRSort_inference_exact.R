MRSort_inference_exact <- function(inputs)
{
  library(MCDA)
  result <- MCDA::MRSortInferenceExact(performanceTable = inputs$performanceTable,
                           assignments = inputs$assignments,
                           categoriesRanks = inputs$categoriesRanks,
                           criteriaMinMax = inputs$criteriaMinMax,
                           veto = inputs$veto,
                           readableWeights = inputs$readableWeights,
                           readableProfiles = inputs$readableProfiles,
                           alternativesIDs = inputs$alternativesIDs,
                           criteriaIDs = inputs$criteriaIDs)
  
  if(result$solverStatus == "Solution found")
  {
    if(inputs$veto)
      return(list(majorityThreshold = result$lambda,
                  criteriaWeights = result$weights,
                  profilesPerformances = result$profilesPerformances,
                  vetoPerformances = result$vetoPerformances))
    else
      return(list(majorityThreshold = result$lambda,
                  criteriaWeights = result$weights,
                  profilesPerformances = result$profilesPerformances))
  }
  else
    stop(result$humanReadableStatus)
}

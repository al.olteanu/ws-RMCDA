checkAndExtractInputs <- function(xmcdaData, programExecutionResult) {
    # all parameters in the order in which the R MCDA function takes them

    performanceTable <- NULL
    assignments <- NULL
    categoriesRanks <- NULL
    criteriaMinMax <- NULL
    veto <- NULL
    alternativesIDs <- NULL
    criteriaIDs <- NULL
    readableWeights <- FALSE
    readableProfiles <- FALSE

    #############################################
    # get criteria
    #############################################
    
    criteriaIDs <- getActiveCriteria(xmcdaData)$criteriaIDs
    
    #############################################
    # get preference directions
    #############################################
    
    criteriaMinMax <- getCriteriaPreferenceDirectionsList(xmcdaData)[[1]]

    #############################################
    # get performance table
    #############################################
    
    performanceTableList <- getNumericPerformanceTableList(xmcdaData)
    
    # we assume that the first performance table is the actual performance table
    
    if(length(performanceTableList) == 0)
      stop("Error: no performance table supplied")
    
    performanceTable <- performanceTableList[[1]]
    
    #############################################
    # get alternatives
    #############################################
    
    activeIDs <- getActiveAlternatives(xmcdaData)$alternativesIDs
    
    alternativesIDs <- rownames(performanceTable)[rownames(performanceTable) %in% activeIDs]
    
    #############################################
    # get program parameters
    #############################################
    
    parameters <- getProgramParametersList(xmcdaData)
    
    if(length(parameters) == 0)
      stop("No parameters provided")
    else
    {
      for(i in 1:length(parameters))
      {
          for(j in 1:length(parameters[[i]]))
          {
            param_name <- names(parameters[[i]])[j]
            if(param_name == "veto")
              veto <- parameters[[i]]$veto[[1]]
            else if(param_name == "readableWeights")
              readableWeights <- parameters[[i]]$readableWeights[[1]]
            else if(param_name == "readableProfiles")
              readableProfiles <- parameters[[i]]$readableProfiles[[1]]
        }
      }
    }
    
    #############################################
    # get categories
    #############################################
    
    categoriesIDs <- getActiveCategories(xmcdaData)$categoriesIDs
    
    #############################################
    # get categories ranks
    #############################################
    
    categoriesRanksList <- getCategoriesValuesList(xmcdaData)
    
    if(length(categoriesRanksList) == 0)
      stop("Error: no categories ranks supplied")
    
    categoriesRanks <- categoriesRanksList[[1]]
    
    if(!all(sort(categoriesRanks) == 1:length(categoriesIDs)))
      stop('Error: categories ranks should be from 1 to the number of categories')
    
    #############################################
    # get assignment examples
    #############################################
    
    alternativesAssignmentsList <- getAlternativesAssignmentsList(xmcdaData)
    
    # we take only the first
    
    if(length(alternativesAssignmentsList) == 0)
      stop("Error: no alternatives assignments supplied")
    
    assignments <- alternativesAssignmentsList[[1]]

    # return results
    
    return(list(performanceTable = performanceTable,
                assignments = assignments,
                categoriesRanks = categoriesRanks,
                criteriaMinMax = criteriaMinMax,
                veto = veto,
                alternativesIDs = alternativesIDs,
                criteriaIDs = criteriaIDs,
                readableWeights = readableWeights,
                readableProfiles = readableProfiles))
}

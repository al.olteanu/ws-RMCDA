<?xml version='1.0' encoding='UTF-8'?>
<program_description>
	<program provider="R-MCDA" name="MRSort_inference_exact" version="1.0" displayName="Exact inference of MRSort" />
	<documentation>
		<description>The MRSort method, a simplification of the Electre TRI method, uses the pessimistic assignment rule, without indifference or preference thresholds attached to criteria. Only a binary discordance condition is considered, i.e. a veto forbids an outranking in any possible concordance situation, or not. The identification of the profiles, weights and majority threshold are done by taking into account assignment examples.</description>
		<contact><![CDATA[Alexandru Olteanu (alexandru.olteanu@univ-ubs.fr)]]></contact>
	</documentation>
	<parameters>

		<input id="inalt" name="alternatives" displayName="alternatives" isoptional="0">
			<documentation>
				<description>A complete list of alternatives to be considered when inferring the MR-Sort model.</description>
			</documentation>
			<xmcda tag="alternatives" />
		</input>

		<input id="inperf" name="performanceTable" displayName="performance table" isoptional="0">
			<documentation>
				<description>The evaluations of the alternatives on the set of criteria.</description>
			</documentation>
			<xmcda tag="performanceTable" />
		</input>

		<input id="incrit" name="criteria" displayName="criteria scales" isoptional="0">
			<documentation>
				<description>A list of criteria on which the alternatives are evaluated. For each criterion, the preference direction should be provided.</description>
			</documentation>
			<xmcda tag="criteriaScales" />
		</input>

		<input id="assignments" name="alternativesAssignments" displayName="alternatives assignments" isoptional="0">
			<documentation>
				<description>The alternatives assignments to categories.</description>
			</documentation>
			<xmcda tag="alternativesAssignments" />
		</input>

		<input id="incategval" name="categoriesRanks" displayName="categories ranks" isoptional="0">
			<documentation>
				<description>A list of categories ranks, 1 stands for the most preferred category and the higher the number the lower the preference for that category.</description>
			</documentation>
			<xmcda tag="categoriesValues" />
		</input>

		<input id="parameters" name="parameters" displayName="parameters" isoptional="defaultTrue">
			<documentation>
				<description>The program parameters.</description>
			</documentation>
			<xmcda tag="programParameters"><![CDATA[
			   
    <programParameters>
        <parameter id="veto">
            <values>
                <value>
                    <boolean>%1</boolean>
                </value>
            </values>
        </parameter>
        <parameter id="readableWeights">
            <values>
                <value>
                    <boolean>%2</boolean>
                </value>
            </values>
        </parameter>
        <parameter id="readableProfiles">
            <values>
                <value>
                    <boolean>%3</boolean>
                </value>
            </values>
        </parameter>
    </programParameters>

			]]></xmcda>
			<gui status="preferGUI">
				<entry id="%1" type="boolean" displayName="Include vetoes">
			        <documentation>
				        <description>An indicator for whether vetoes should be included in the model or not.</description>
			        </documentation>
					<defaultValue>false</defaultValue>
				</entry>
				<entry id="%2" type="boolean" displayName="Readable weights">
					<documentation>
						<description>An indicator for whether the weights should made easier to read.</description>
					</documentation>
					<defaultValue>false</defaultValue>
				</entry>
				<entry id="%3" type="boolean" displayName="Readable profiles">
					<documentation>
						<description>An indicator for whether the profiles should made easier to read.</description>
					</documentation>
					<defaultValue>false</defaultValue>
				</entry>
			</gui>
		</input>

		<output id="outcatprofpt" name="categoriesProfilesPerformanceTable" displayName="categories profiles performance table">
			<documentation>
				<description>The evaluations of the category profiles.</description>
			</documentation>
			<xmcda tag="performanceTable" />
		</output>

		<output id="outcatvetopt" name="vetoProfilesPerformanceTable" displayName="veto profiles performance table">
			<documentation>
				<description>The evaluations of the veto profiles.</description>
			</documentation>
			<xmcda tag="performanceTable" />
		</output>

		<output id="weights" name="criteriaWeights" displayName="criteria weights">
			<documentation>
				<description>The criteria weights.</description>
			</documentation>
			<xmcda tag="criteriaValues" />
		</output>

		<output id="outcatprof" name="categoriesProfiles" displayName="categories profiles">
			<documentation>
				<description>The categories delimiting profiles.</description>
			</documentation>
			<xmcda tag="categoriesProfiles" />
		</output>

		<output id="outcatveto" name="vetoProfiles" displayName="veto profiles">
			<documentation>
				<description>The categories veto profiles.</description>
			</documentation>
			<xmcda tag="categoriesProfiles" />
		</output>

		<output id="majority" name="majorityThreshold" displayName="majority threshold">
			<documentation>
				<description>The majority threshold.</description>
			</documentation>
			<xmcda tag="programParameters" />
		</output>

		<output id="msg" name="messages" displayName="messages">
			<documentation>
				<description>Messages from the execution of the webservice. Possible errors in the input data will be given here.</description>
			</documentation>
			<xmcda tag="programExecutionResult" />
		</output>

	</parameters>
</program_description>
